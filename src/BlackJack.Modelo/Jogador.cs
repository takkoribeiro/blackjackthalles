﻿using System.Collections.Generic;

namespace BlackJack.Modelo
{
    public class Jogador
    {
        public Jogador() { }

        public Jogador(bool dealer)
        {
            this.Pontuacao = 0;
            this.PontuacaoVisivel = 0;
            this.Cartas = new List<Carta>();
            this.Dealer = dealer;
        }

        public int Pontuacao { get; set; }
        public int PontuacaoVisivel { get; set; }
        public List<Carta> Cartas { get; set; }
        public bool Dealer { get; set; }
    }
}
