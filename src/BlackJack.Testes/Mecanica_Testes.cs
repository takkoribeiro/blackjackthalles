﻿using BlackJack.Modelo;
using Xunit;

namespace BlackJack.Testes
{
    [Collection("Mecanica")]
    public class Mecanica_Testes
    {
        #region | Baralho |
        [Fact]
        public void NovoBaralhoCom52Cartas()
        {
            Baralho baralho = new Baralho(52);
            Assert.True(baralho.QuantidadeCartas == 52);
            Assert.True(baralho.Cartas.Count == 52);
        }

        [Fact]
        public void NovoBaralhoCom104Cartas()
        {
            Baralho baralho = new Baralho(104);
            Assert.True(baralho.QuantidadeCartas == 104);
            Assert.True(baralho.Cartas.Count == 104);
        }

        #endregion

        #region | Cartas |
        [Fact]
        public void NovaCartaPaus()
        {
            Carta carta = new Carta(Naipe.Paus, Valor.As);
            Assert.Equal(Naipe.Paus, carta.Naipe);
        }

        [Fact]
        public void NovaCartaOuros()
        {
            Carta carta = new Carta(Naipe.Ouros, Valor.As);
            Assert.Equal(Naipe.Ouros, carta.Naipe);
        }

        [Fact]
        public void NovaCartaCopas()
        {
            Carta carta = new Carta(Naipe.Copas, Valor.As);
            Assert.Equal(Naipe.Copas, carta.Naipe);
        }

        [Fact]
        public void NovaCartaEspadas()
        {
            Carta carta = new Carta(Naipe.Espadas, Valor.As);
            Assert.Equal(Naipe.Espadas, carta.Naipe);
        }

        #endregion

        #region | Jogador |
        [Fact]
        public void NovoJogadorSemCartas()
        {
            Jogador jogador = new Jogador(false);
            Assert.True(jogador.Cartas.Count == 0);
        }

        [Fact]
        public void NovoJogadorSemPontuacao()
        {
            Jogador jogador = new Jogador(false);
            Assert.True(jogador.Pontuacao == 0);
        }

        [Fact]
        public void NovoJogadorNaoDealer()
        {
            Jogador jogador = new Jogador(false);
            Assert.False(jogador.Dealer);
        }

        [Fact]
        public void NovoDealer()
        {
            Jogador jogador = new Jogador(true);
            Assert.True(jogador.Dealer);
        }

        #endregion

        #region | Partida |
        [Fact]
        public void NovaPartidaPossuiBaralhoComCartas()
        {
            Partida partida = new Partida(1);
            Assert.True(partida.Baralho.Cartas.Count > 0);
        }

        [Fact]
        public void NovaPartidaPossuiJogador()
        {
            Partida partida = new Partida(2);
            Assert.NotNull(partida.Jogador);
        }

        [Fact]
        public void NovaPartidaPossuiDealer()
        {
            Partida partida = new Partida(3);
            Assert.NotNull(partida.Dealer);
        }

        [Fact]
        public void NovaPartidaPossuiFaseInicial()
        {
            Partida partida = new Partida(1);
            Assert.True(partida.FaseAtual == Fase.Inicial);
        }

        [Fact]
        public void NovaPartidaPossuiEventoEmbaralhar()
        {
            Partida partida = new Partida(1);
            Assert.True(partida.EventoAtual == Evento.Embaralhar);
        }

        #endregion
    }
}

