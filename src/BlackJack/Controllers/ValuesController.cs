﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BlackJack.API.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] 
            {
                "Bem-vindo ao jogo BlackJack - CAPCO - Thalles Prado Ribeiro." ,
                "Utilizar a URL a seguir para jogar: http://localhost:51129/api/blackjack",
                " 1 - INICIAR NOVA PARTIDA -> GET",
                " 2 - COMPRAR NOVA CARTA -> PUT - id = Código da Partida",
                " 3 - FINALIZAR PARTIDA -> POST - id = Código da Partida"
            };
        }
    }
}
