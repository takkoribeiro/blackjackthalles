﻿using BlackJack.Modelo;
using BlackJack.Negocio;
using System;
using Xunit;

namespace BlackJack.Testes
{
    [Collection("Cenarios")]
    public class Cenarios_Testes
    {
        [Fact]
        public void NovaPartidaRetornaCodigoPartida()
        {
            Assert.True(NegocioBlackJack.NovaPartida(1).CodigoPartida == 1);
        }

        [Fact]
        public void NovaPartidaRetornaHorarioInicio()
        {
            Assert.IsType(typeof(DateTime), NegocioBlackJack.NovaPartida(2).HorarioInicio);
        }

        [Fact]
        public void NovaPartidaRetornaCartasJogador()
        {
            Assert.True(NegocioBlackJack.NovaPartida(3).Jogador.Cartas.Count == 2);
        }

        [Fact]
        public void NovaPartidaRetornaCartasDealer()
        {
            Assert.True(NegocioBlackJack.NovaPartida(4).Dealer.Cartas.Count == 2);
        }

        [Fact]
        public void NovaPartidaRetornaNaoFinalizado()
        {
            Assert.False(NegocioBlackJack.NovaPartida(5).Finalizada);
        }

        [Fact]
        public void CompraCartaJogador()
        {
            Partida partida = NegocioBlackJack.NovaPartida(6);
            NegocioBlackJack.ComprarCarta(ref partida);
            Assert.True(partida.Jogador.Cartas.Count == 3);
        }

        [Fact]
        public void FinalizaAcoesJogador()
        {
            Partida partida = NegocioBlackJack.NovaPartida(7);
            NegocioBlackJack.Finalizar(ref partida);
            Assert.True(partida.Finalizada);
        }
    }
}
