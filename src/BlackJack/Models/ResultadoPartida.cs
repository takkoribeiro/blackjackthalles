﻿using BlackJack.Modelo;
using System;
using System.Collections.Generic;

namespace BlackJack.API.Models
{
    public class ResultadoPartida
    {
        public int CodigoPartida { get; set; }
        public DateTime HorarioInicio { get; set; }
        public DateTime HorarioFim { get; set; }
        public List<Carta> SuasCartas { get; set; }
        public List<Carta> CartasDealer { get; set; }
        public int SuaPontuacao { get; set; }
        public int PontuacaoDealer { get; set; }
        public bool PartidaFinalizada { get; set; }
        public string Vencedor { get; set; }
    }
}

