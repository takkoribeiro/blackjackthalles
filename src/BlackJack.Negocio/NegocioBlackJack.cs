﻿using BlackJack.Modelo;
using System;
using System.Linq;

namespace BlackJack.Negocio
{
    public static class NegocioBlackJack
    {
        private const int PONTUACAO_VITORIA = 21;

        #region | Ações do Jogador |

        /// <summary>
        /// Método para iniciar nova partida com um código específico.
        /// </summary>
        /// <param name="codigoPartida">Código da partida</param>
        /// <returns>Retorna a partida</returns>
        public static Partida NovaPartida(int codigoPartida)
        {
            Partida novaPartida = new Partida(codigoPartida);
            FaseInicial(ref novaPartida);
            FaseValidacao(ref novaPartida);
            return novaPartida;
        }

        /// <summary>
        /// Método para comprar nova carta para o jogador
        /// </summary>
        /// <param name="partida">Partida em andamento</param>
        /// <returns>Retorna a partida atualizada</returns>
        public static Partida ComprarCarta(ref Partida partida)
        {
            FaseCompras(ref partida);
            FaseValidacao(ref partida);
            return partida;
        }

        /// <summary>
        /// Método para finalizar as ações do jogador
        /// </summary>
        /// <param name="partida">Partida em andamento</param>
        /// <returns>Retorna a partida finalizada</returns>
        public static Partida Finalizar(ref Partida partida)
        {
            partida.FaseAtual = Fase.Final;
            //Revela qualquer carta ainda não revelada do dealer
            int indiceCarta = partida.Dealer.Cartas.FindIndex(x => !x.Revelada);
            if (indiceCarta > 0)
                partida.Dealer.Cartas[indiceCarta].Revelada = true;

            //Efetua compras de carta para o dealer se a pontuação menor que 17 e quantidade de cartas menor que 5
            while (partida.Dealer.Pontuacao <= 17 && partida.Dealer.Cartas.Count < 5)
            {
                Carta cartaDealer = ComprarCartaBaralho(ref partida, true);
                partida.Dealer.Cartas.Add(cartaDealer);
                ContabilizarPontos(ref partida);
            }

            //Valida se há vencedor
            FaseValidacao(ref partida);

            //Caso ainda não exista vencedor, verifica quem se aproximou mais da pontuação vitoriosa
            if (!partida.Finalizada)
            {
                if (partida.Jogador.Pontuacao == partida.Dealer.Pontuacao)
                    partida.Vencedor = Vencedor.Empate;
                else if (partida.Jogador.Pontuacao > partida.Dealer.Pontuacao)
                    partida.Vencedor = Vencedor.Jogador;
                else
                    partida.Vencedor = Vencedor.Dealer;

                partida.Finalizada = true;
                partida.HorarioFim = DateTime.Now;
            }

            return partida;
        }

        #endregion

        #region | Fases Principais |

        //Método para iniciar uma nova partida. Embaralha, distribui as cartas e contabiliza a pontuação inicial
        private static void FaseInicial(ref Partida partida)
        {
            partida.FaseAtual = Fase.Inicial;
            Embaralhar(ref partida);
            Distribuir(ref partida);
            ContabilizarPontos(ref partida);
        }

        //Método para verificar se a partida será finalizada ou não
        private static void FaseValidacao(ref Partida partida)
        {
            partida.FaseAtual = partida.FaseAtual != Fase.Final ? Fase.Validacao : Fase.Final;
            Vencedor vencedor = ChecarVencedor(ref partida);

            //Se não encontrar vencedor, procura por um perdedor, se houver perdedor força a finalização da partida
            if (vencedor == Vencedor.Nenhum)
            {
                vencedor = ChecarPerdedor(ref partida);
                partida.FaseAtual = vencedor != Vencedor.Nenhum ? Fase.Final : partida.FaseAtual;
            }

            if (partida.FaseAtual == Fase.Final)
            {
                partida.Finalizada = vencedor != Vencedor.Nenhum;
                partida.HorarioFim = vencedor != Vencedor.Nenhum ? (DateTime?)DateTime.Now : null;
                partida.Vencedor = vencedor;
            }
        }

        //Método para comprar nova carta para o jogador
        private static void FaseCompras(ref Partida partida)
        {
            partida.FaseAtual = Fase.Compras;
            Carta cartaJogador = ComprarCartaBaralho(ref partida, true);
            partida.Jogador.Cartas.Add(cartaJogador);
            ContabilizarPontos(ref partida);
        }

        #endregion

        #region | Métodos Auxiliares |

        //Método para embaralhar as cartas do baralho
        private static void Embaralhar(ref Partida partida)
        {
            partida.EventoAtual = Evento.Embaralhar;
            partida.Baralho.Cartas.Shuffle();
        }

        //Método para distribuir as cartas entre os jogadores da partida (duas para cada um)
        private static void Distribuir(ref Partida partida)
        {
            partida.EventoAtual = Evento.Distribuir;
            partida.Jogador.Cartas.Add(ComprarCartaBaralho(ref partida, true));
            partida.Dealer.Cartas.Add(ComprarCartaBaralho(ref partida, true));
            partida.Jogador.Cartas.Add(ComprarCartaBaralho(ref partida, true));

            //Se a primeira carta do dealer for Às ou uma carta de valor 10, revela a segunda carta também
            partida.Dealer.Cartas.Add(ComprarCartaBaralho(ref partida, partida.Dealer.Cartas.First().Valor == Valor.As || Convert.ToInt32(partida.Dealer.Cartas.First().Valor.GetCategory()) == 10));
        }

        //Método para comprar a próxima carta do baralho
        private static Carta ComprarCartaBaralho(ref Partida partida, bool revelar)
        {
            Carta carta = partida.Baralho.Cartas.FirstOrDefault();
            partida.Baralho.Cartas.Remove(carta);
            carta.Revelada = revelar;
            return carta;
        }

        //Método para contabilizar a pontuação dos jogadores
        private static void ContabilizarPontos(ref Partida partida)
        {
            int pontuacao = 0;
            //Ordena pelas cartas mais altas (deixando o Às por último para facilitar a validação)
            foreach (Carta carta in partida.Jogador.Cartas.OrderByDescending(x => (int)x.Valor))
            {
                //Se a carta for um Às e existir uma outra carta de valor 10, o Às deverá valer 11 desde que não ultrapasse o limite da pontuação vitoriosa
                if (carta.Valor == Valor.As && partida.Jogador.Cartas.Any(x => Convert.ToInt32(x.Valor.GetCategory()) == 10) && pontuacao + 11 <= PONTUACAO_VITORIA)
                    pontuacao += 11;
                //Se não, recupera o valor da carta pelo enumerador
                else
                    pontuacao += Convert.ToInt32(carta.Valor.GetCategory());
            }
            partida.Jogador.Pontuacao = pontuacao;
            partida.Jogador.PontuacaoVisivel = pontuacao;

            pontuacao = 0;
            int pontuacaoVisivel = 0;
            //Ordena pelas cartas mais altas (deixando o Às por último para facilitar a validação)
            foreach (Carta carta in partida.Dealer.Cartas.OrderByDescending(x => (int)x.Valor))
            {
                //Se a carta for um Às e existir uma outra carta de valor 10, o Às deverá valer 11 desde que não ultrapasse o limite da pontuação vitoriosa
                if (carta.Valor == Valor.As && partida.Dealer.Cartas.Any(x => Convert.ToInt32(x.Valor.GetCategory()) == 10) && pontuacao + 11 <= PONTUACAO_VITORIA)
                {
                    pontuacao += 11;
                    pontuacaoVisivel += carta.Revelada ? 11 : 0;
                }
                //Se não, recupera o valor da carta pelo enumerador
                else
                {
                    pontuacao += Convert.ToInt32(carta.Valor.GetCategory());
                    pontuacaoVisivel += carta.Revelada ? Convert.ToInt32(carta.Valor.GetCategory()) : 0;
                }

            }
            partida.Dealer.Pontuacao = pontuacao;
            partida.Dealer.PontuacaoVisivel = pontuacaoVisivel;
        }

        //Método para checar se há um vencedor na partida
        private static Vencedor ChecarVencedor(ref Partida partida)
        {
            partida.EventoAtual = Evento.ChecarVencedor;
            bool jogadorVitorioso = partida.Jogador.Pontuacao == PONTUACAO_VITORIA;
            bool dealerVitorioso = partida.Dealer.Pontuacao == PONTUACAO_VITORIA;

            if (jogadorVitorioso && dealerVitorioso)
                return Vencedor.Empate;

            return jogadorVitorioso ? Vencedor.Jogador : dealerVitorioso ? Vencedor.Dealer : Vencedor.Nenhum;
        }

        //Método para checar se há um perdedor na partida. Retorna o vencedor caso encontre o perdedor
        private static Vencedor ChecarPerdedor(ref Partida partida)
        {
            partida.EventoAtual = Evento.ChecarPerdedor;
            bool jogadorPerdedor = partida.Jogador.Pontuacao > PONTUACAO_VITORIA;
            bool dealerPerdedor = partida.Dealer.Pontuacao > PONTUACAO_VITORIA;

            if (jogadorPerdedor && dealerPerdedor)
                return Vencedor.Empate;

            return jogadorPerdedor ? Vencedor.Dealer : dealerPerdedor ? Vencedor.Jogador : Vencedor.Nenhum;
        }

        #endregion
    }
}
