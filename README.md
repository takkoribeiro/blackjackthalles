#BlackJack - Desafio técnico CAPCO
Thalles Prado Ribeiro

#Utilizei o VS2015, Web API e .NET Core
Não criei interface, mas a API está funcional e pode-se utilizar de forma simples os métodos:

#GET 
Iniciar nova partida;

#PUT 
Comprar nova carta (informar código da partida via id);

#POST 
Finalizar as ações do jogador (informar código da partida via id).

Também não me preocupei com base de dados (persistência das informações) ou concorrência de threads, então as partidas existem apenas em memória (enquanto a API estiver de pé).
Apesar de ter desenvolvido os testes unitários, não consegui executá-los e fazer o framework funcionar a tempo utilizando .NET Core por algum problema de compatibilidade =/