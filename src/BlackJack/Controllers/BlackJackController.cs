﻿using BlackJack.API.Models;
using BlackJack.Modelo;
using BlackJack.Negocio;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BlackJack.API.Controllers
{
    [Route("api/[controller]")]
    public class BlackJackController : Controller
    {
        public static List<Partida> partidas = new List<Partida>();

        /// <summary>
        /// Método para iniciar uma nova partida
        /// </summary>
        /// <returns>Retorna o resultado da partida até o momento</returns>
        [HttpGet]
        public JsonResult Get()
        {
            //Cria nova partida e adiciona na lista de partidas
            int proximaPartida = partidas.Count + 1;
            Partida novaPartida = NegocioBlackJack.NovaPartida(proximaPartida);
            partidas.Add(novaPartida);

            JsonResult retorno = ResultadoPartida(novaPartida);
            return retorno;
        }

        /// <summary>
        /// Método para comprar uma nova carta para o jogador
        /// </summary>
        /// <param name="id">Código da partida</param>
        /// <returns>Retorna o resultado da partida até o momento</returns>
        [HttpPut]
        public JsonResult Put(int id)
        {
            Partida partida = partidas.Find(x => x.CodigoPartida == id);
            if (partida == null || partida.Finalizada)
                return new JsonResult("Partida não encontrada ou já finalizada.");

            //Remove da lista temporariamente
            partidas.Remove(partida);

            //Compra cartas e retorna a partida para a lista
            NegocioBlackJack.ComprarCarta(ref partida);
            partidas.Add(partida);

            JsonResult retorno = ResultadoPartida(partida);
            return retorno;
        }

        /// <summary>
        /// Método para finalizar as ações do jogador, ou seja, não efetuará novas compras de cartas
        /// </summary>
        /// <param name="id">Código da partida</param>
        /// <returns>Retorna o resultado da partida</returns>
        [HttpPost]
        public JsonResult Post(int id)
        {
            Partida partida = partidas.Find(x => x.CodigoPartida == id);
            if (partida == null || partida.Finalizada)
                return new JsonResult("Partida não encontrada ou já finalizada.");

            //Remove da lista temporariamente
            partidas.Remove(partida);

            //Compra cartas e retorna a partida para a lista
            NegocioBlackJack.Finalizar(ref partida);
            partidas.Add(partida);

            JsonResult retorno = ResultadoPartida(partida);
            return retorno;
        }

        #region | Métodos auxiliares |
        private JsonResult ResultadoPartida(Partida partida)
        {
            return new JsonResult(new Resultado(partida));
        }
        #endregion
    }
}
