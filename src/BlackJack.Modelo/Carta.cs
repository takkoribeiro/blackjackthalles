﻿using System.ComponentModel;

namespace BlackJack.Modelo
{
    public class Carta
    {
        public Carta() { }

        public Carta(Naipe naipe, Valor valor)
        {
            this.Naipe = naipe;
            this.Valor = valor;
            this.Revelada = false;
        }

        public Naipe Naipe { get; set; }
        public Valor Valor { get; set; }
        public bool Revelada { get; set; }
    }

    public enum Naipe
    {
        [Description("Copas")]
        Copas = 1,
        [Description("Ouros")]
        Ouros = 2,
        [Description("Paus")]
        Paus = 3,
        [Description("Espadas")]
        Espadas = 4
    }

    public enum Valor
    {
        [Description("Às")]
        [Category("1")]
        As = 1,
        [Description("Dois")]
        [Category("2")]
        Dois = 2,
        [Description("Três")]
        [Category("3")]
        Tres = 3,
        [Description("Quatro")]
        [Category("4")]
        Quatro = 4,
        [Description("Cinco")]
        [Category("5")]
        Cinco = 5,
        [Description("Seis")]
        [Category("6")]
        Seis = 6,
        [Description("Sete")]
        [Category("7")]
        Sete = 7,
        [Description("Oito")]
        [Category("8")]
        Oito = 8,
        [Description("Nove")]
        [Category("9")]
        Nove = 9,
        [Description("Dez")]
        [Category("10")]
        Dez = 10,
        [Description("Valete")]
        [Category("10")]
        Valete = 11,
        [Description("Dama")]
        [Category("10")]
        Dama = 12,
        [Description("Rei")]
        [Category("10")]
        Rei = 13
    }
}
