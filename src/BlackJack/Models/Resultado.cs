﻿using BlackJack.Modelo;
using System;
using System.Collections.Generic;

namespace BlackJack.API.Models
{
    public class Resultado
    {
        public Resultado() { }

        public Resultado(Partida partida)
        {
            this.CodigoPartida = partida.CodigoPartida;
            this.HorarioInicio = partida.HorarioInicio;
            this.HorarioFim = partida.HorarioFim;

            this.SuasCartas = new List<ResultadoCarta>();
            partida.Jogador.Cartas.ForEach(x => this.SuasCartas.Add(new ResultadoCarta(x)));

            this.CartasDealer = new List<ResultadoCarta>();
            partida.Dealer.Cartas.ForEach(x => this.CartasDealer.Add(new ResultadoCarta(x)));

            this.SuaPontuacao = partida.Jogador.PontuacaoVisivel;
            //Apenas mostra a pontuação de cartas reveladas do dealer (desconsiderando a pontuação de alguma carta não revelada)
            this.PontuacaoDealer = partida.Dealer.PontuacaoVisivel;

            this.PartidaFinalizada = partida.Finalizada ? "Sim" : "Não";
            this.Vencedor = partida.Vencedor == Modelo.Vencedor.Nenhum ? "Nenhum" : (int)partida.Vencedor == 1 ? "Jogador" : "Dealer";
        }

        public int CodigoPartida { get; set; }
        public DateTime HorarioInicio { get; set; }
        public DateTime? HorarioFim { get; set; }
        public List<ResultadoCarta> SuasCartas { get; set; }
        public List<ResultadoCarta> CartasDealer { get; set; }
        public int SuaPontuacao { get; set; }
        public int PontuacaoDealer { get; set; }
        public string PartidaFinalizada { get; set; }
        public string Vencedor { get; set; }
    }

    public class ResultadoCarta
    {
        public ResultadoCarta() { }

        public ResultadoCarta(Carta carta)
        {
            this.Naipe = carta.Revelada ? carta.Naipe.GetDescription() : "Ainda não revelado";
            this.Valor = carta.Revelada ? carta.Valor.GetDescription() : "Ainda não revelado";
        }

        public string Naipe { get; set; }
        public string Valor { get; set; }
    }
}

