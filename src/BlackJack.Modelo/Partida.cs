﻿using System;
using System.ComponentModel;

namespace BlackJack.Modelo
{
    public class Partida
    {

        public Partida() { }

        public Partida(int codigoPartida)
        {
            this.CodigoPartida = codigoPartida;
            this.HorarioInicio = DateTime.Now;
            this.Baralho = new Baralho(52);
            this.Jogador = new Jogador(false);
            this.Dealer = new Jogador(true);
            this.Finalizada = false;
            this.Vencedor = Vencedor.Nenhum;
            this.FaseAtual = Fase.Inicial;
            this.EventoAtual = Evento.Embaralhar;
        }

        public int CodigoPartida { get; set; }
        public DateTime HorarioInicio { get; set; }
        public DateTime? HorarioFim { get; set; }
        public Baralho Baralho { get; set; }
        public Jogador Jogador { get; set; }
        public Jogador Dealer { get; set; }
        public bool Finalizada { get; set; }
        public Vencedor Vencedor { get; set; }
        public Fase FaseAtual { get; set; }
        public Evento EventoAtual { get; set; }
    }

    public enum Vencedor
    {
        [Description("Jogador")]
        Jogador = 1,
        [Description("Dealer")]
        Dealer = 2,
        [Description("Empate")]
        Empate = 3,
        [Description("Nenhum")]
        Nenhum = 4
    }

    public enum Fase
    {
        Inicial,
        Validacao,
        Compras,
        Final
    }

    public enum Evento
    {
        Embaralhar,
        Distribuir,
        ChecarVencedor,
        ChecarPerdedor
    }
}
