﻿using System.Collections.Generic;

namespace BlackJack.Modelo
{
    public class Baralho
    {
        public Baralho() { }

        public Baralho(int quantidadeCartas)
        {
            this.QuantidadeCartas = quantidadeCartas;
            this.Cartas = new List<Carta>();
            this.PreencherBaralho(quantidadeCartas);
        }

        public int QuantidadeCartas { get; set; }
        public List<Carta> Cartas { get; set; }

        private void PreencherBaralho(int quantidadeCartas)
        {
            Naipe naipe;
            Valor valor;
            int quantidadePorNaipe = quantidadeCartas / 4;

            for (int i = 1; i <= 4; i++)
            {
                naipe = (Naipe)i;
                for (int j = 1; j <= quantidadePorNaipe; j++)
                {
                    valor = (Valor)j;
                    this.Cartas.Add(new Carta(naipe, valor));
                }
            }
        }
    }
}
